package org.ksondzyk.utils;

import lombok.extern.slf4j.Slf4j;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class PropertyReader {

    private static PropertyReader instance = null;
    private Properties properties = new Properties();

    PropertyReader() {
        String appName = System.getProperty("appName", "app");
        try {
            properties.load(new FileInputStream("src/main/resources/%s.properties".formatted(appName)));
        } catch (IOException e) {
            log.error("Something wrong with file!!!!");
            throw new RuntimeException(e);
        }
    }

    public static PropertyReader getInstance() {
        if (instance == null){
            instance = new PropertyReader();
        }
        return instance;
    }

    public static String getProperty(String propertyName){
        return getInstance().properties.getProperty(propertyName);
    }
}


