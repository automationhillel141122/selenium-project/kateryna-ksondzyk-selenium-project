package org.ksondzyk.utils;

import org.aeonbits.owner.Config;
@Config.Sources("file:src/main/resources/app.properties")
public interface ConfigProperties extends Config {
    @Key("base.url")
    String baseUrl();

    @Key("user.name")
    String userName();

    @Key("user.password")
    String userPassword();
}

