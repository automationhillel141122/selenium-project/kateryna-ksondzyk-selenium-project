package org.ksondzyk.driver;

public enum WebdriverType {
    CHROME,
    FIREFOX,
    SAFARI
}

