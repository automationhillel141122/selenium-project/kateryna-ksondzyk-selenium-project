package org.ksondzyk.pages;

public enum AppCurrency {
    USD("US Dollar"),
    EUR("Euro"),
    JPE("Japanese Yen");

    private String currencyText;

    AppCurrency(String currencyText) {
        this.currencyText = currencyText;
    }

    public String getCurrencyText() {
        return currencyText;
    }
}
