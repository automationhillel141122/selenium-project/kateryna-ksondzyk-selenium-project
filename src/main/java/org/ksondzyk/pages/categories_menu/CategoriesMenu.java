package org.ksondzyk.pages.categories_menu;

import org.ksondzyk.driver.WebdriverHolder;
import org.ksondzyk.pages.BasePage;
import org.ksondzyk.pages.products.ProductsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CategoriesMenu {
    public BasePage selectMainCategories(String mainCategories){
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[starts-with(.,'%s')]".formatted(mainCategories)))
                .click();
        return new BasePage();
    }

    public ProductsPage selectSubCategories(String mainCategories, String subCategories){
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement mainCategoriesElement = driver.findElement(By.xpath("//ul[@class='top-menu notmobile']/li/a[starts-with(.,'%s')]".formatted(mainCategories)));

        Actions actions = new Actions(driver);
        actions
                .moveToElement(mainCategoriesElement)
                .build()
                .perform();

        mainCategoriesElement
                .findElement(By.xpath("./.."))
                .findElement(By.xpath("//ul[@class='sublist first-level']/li/a[starts-with(.,'%s')]".formatted(subCategories)))
                .click();

        return new ProductsPage();

    }
}
