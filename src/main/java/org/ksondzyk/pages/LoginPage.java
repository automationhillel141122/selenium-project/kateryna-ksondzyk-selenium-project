package org.ksondzyk.pages;

import org.ksondzyk.driver.WebdriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindBy(id = "Email")
    WebElement emailInput;

    @FindBy(id = "Password")
    WebElement passwordInput;

    @FindBy(id = "RememberMe")
    WebElement rememberMeCheckbox;

    @FindBy(css = ".button-1.login-button")
    WebElement loginButton;

    public LoginPage() {
        super();
    }

    public BasePage login(String email, String password, boolean... rememberMe) {
        enterCredentialsAndLogin(email,password,rememberMe);
        return new BasePage();
    }

    public LoginPage unSuccesslogin(String email, String password, boolean... rememberMe) {
        enterCredentialsAndLogin(email,password,rememberMe);
        return new LoginPage();
    }

    private void enterCredentialsAndLogin(String email, String password, boolean... rememberMe){
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        if (rememberMe.length > 0) {
            selectRememberMe(rememberMe[0]);
        }
        loginButton.click();
    }

    public String getEmailErrorText(){
        return WebdriverHolder
                .getInstance()
                .getDriver()
                .findElement(By.id("Email-error"))
                .getText();
    }

    public String getSummuryErrorText(){
        return WebdriverHolder
                .getInstance()
                .getDriver()
                .findElement(By.cssSelector(".message-error.validation-summary-errors"))
                .getText();
    }

    private void selectRememberMe(boolean rememberMe){
            if (rememberMe) {
                if (!rememberMeCheckbox.isSelected()) {
                    rememberMeCheckbox.click();
                }
            } else {
                if (rememberMeCheckbox.isSelected()) {
                    rememberMeCheckbox.click();
                }
            }
        }


}
