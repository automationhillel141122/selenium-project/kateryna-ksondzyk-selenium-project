package org.ksondzyk.pages.main_menu;

import org.ksondzyk.driver.WebdriverHolder;
import org.openqa.selenium.By;

public class MainMenu {

    public void selectLogin(){
        selectMenuItem(MainMenuItem.LOGIN);
    }

    public void selectRegister(){
        selectMenuItem(MainMenuItem.REGISTER);
    }

    public void selectWishList(){
        selectMenuItem(MainMenuItem.WISH_LIST);
    }

    public void selectShoppingCart(){
        selectMenuItem(MainMenuItem.SHOPPING_CART);
    }

    public void selectAccount(){
        selectMenuItem(MainMenuItem.ACCOUNT);
    }

    public void selectLogout(){
        selectMenuItem(MainMenuItem.LOGOUT);
    }

    private void selectMenuItem(MainMenuItem menuItem){
        WebdriverHolder.getInstance().getDriver()
                .findElement(By.cssSelector("a.ico-"+ menuItem.value()))
                .click();
    }

}
