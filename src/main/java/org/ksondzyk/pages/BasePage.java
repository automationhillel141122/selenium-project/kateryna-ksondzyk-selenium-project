package org.ksondzyk.pages;

import org.ksondzyk.driver.WebdriverHolder;
import org.ksondzyk.pages.categories_menu.CategoriesMenu;
import org.ksondzyk.pages.main_menu.MainMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class BasePage {

    public BasePage() {
        PageFactory.initElements(WebdriverHolder.getInstance().getDriver(), this);
    }

    public MainMenu mainMenu(){
        return new MainMenu();
    }

    public CategoriesMenu categoriesMenu(){
        return new CategoriesMenu();
    }

    public BasePage selectCurrency(AppCurrency currency){
        WebElement customerCurrency = WebdriverHolder.getInstance().getDriver()
                .findElement(By.id("customerCurrency"));
        new Select(customerCurrency)
                .selectByVisibleText(currency.getCurrencyText());
        return new BasePage();
    }

    public BasePage search(String searchPattern){
        WebElement searchForm = WebdriverHolder.getInstance().getDriver().findElement(By.id("small-searchterms"));
        searchForm.findElement(By.xpath(".//input")).sendKeys(searchPattern);
        searchForm.findElement(By.xpath(".//button")).click();

        return new BasePage();
    }
}
