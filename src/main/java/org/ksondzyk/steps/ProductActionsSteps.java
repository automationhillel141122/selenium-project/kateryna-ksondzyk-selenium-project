package org.ksondzyk.steps;

import org.ksondzyk.pages.BasePage;
import org.ksondzyk.pages.products.ProductsPage;

public class ProductActionsSteps {
    public void addProductToCart(String category, String subCategory, String productName){
        ProductsPage productsPage;
        if (subCategory == null){
            new BasePage().categoriesMenu().selectMainCategories(category);
        } else {
            new BasePage().categoriesMenu().selectSubCategories(category, subCategory);
        }
        productsPage = new ProductsPage();

        productsPage.addProductToCart(productName);
    }
}
