package org.ksondzyk.tests.listeners;

import io.qameta.allure.Attachment;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.ksondzyk.driver.WebdriverHolder;
import org.ksondzyk.utils.MyFileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Slf4j
public class MyTestListener implements ITestListener {

    // static Logger log = LoggerFactory.getLogger(MyTestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test: " + result.getName() + " started!");
        ITestListener.super.onTestStart(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log.info("Test: " + result.getName() + " finished successfully!");
        ITestListener.super.onTestSuccess(result);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.error("Test: " + result.getName() + " finished failed!");
        makeScreenshot(result);
        ITestListener.super.onTestFailure(result);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.info("Test: " + result.getName() + " finished skipped!");
        ITestListener.super.onTestSkipped(result);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        ITestListener.super.onTestFailedButWithinSuccessPercentage(result);
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        ITestListener.super.onTestFailedWithTimeout(result);
    }

    @Override
    public void onStart(ITestContext context) {
        ITestListener.super.onStart(context);
    }

    @Override
    public void onFinish(ITestContext context) {
        ITestListener.super.onFinish(context);
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] makeScreenshot(ITestResult testResult){
        log.info("Making screenshot...");
        File screenshotAs = ((TakesScreenshot) WebdriverHolder.getInstance().getDriver())
                .getScreenshotAs(OutputType.FILE);
        File screenshot = new File(MyFileUtils.DirectoryFor.SCREENSHOTS.getDirName(),
                testResult.getName() + new Date().getTime() + ".png");

        try {
            FileUtils.copyFile(screenshotAs, screenshot);
            log.info("Screenshot saved.\nScreenshot path: " + screenshot.getAbsolutePath());
            return FileUtils.readFileToByteArray(screenshot);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


